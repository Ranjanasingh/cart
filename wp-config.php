<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cart');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4~/2yJ>:tZ @H1a3S&h2U5#uw%K#:>%P)r%RO!o,s{$#@Tk UqYLnx.*ANI)+qH$');
define('SECURE_AUTH_KEY',  'C+i2`:9Mmu`rCu-~`Fvx|F_ng$^Bb+0)TT[G]S0$g^&4LcX]6PW2><S:W<,e !C7');
define('LOGGED_IN_KEY',    'II&G-iBboImL-n%yP1+[7:=}tkXj`75qU*A-l!p`~c{ zWl|re vg/U188|T[s:$');
define('NONCE_KEY',        ' a`WYNWs>1Ja2yWWE=fp]yi*eVN+`J|Gg5QxiTC/tWu~-TECRHn(-u-2FTlZBB]=');
define('AUTH_SALT',        'Z^ypuwaSb;?W#*<nW~9K`@&d(cHTX+nrsH@jL*:3(H+.~8Cg0PU(TsR%4r9-DjH`');
define('SECURE_AUTH_SALT', 'W;Cx-;M0[je.URUa7xpOjCzB9A(ozo!|v&=/@m5/^!ZnOV9|N6[COj@ZPs,!#+D|');
define('LOGGED_IN_SALT',   'ieo3k5O4N.8h?i`_cIvUwP:^Hgt-jD+k5T!@Q h2k+%yT7H7I$R+D]qr+Tnh?78R');
define('NONCE_SALT',       '=-=6n.(rRpd5wzIr+a69/Sn}YlD!4U+A=agoU+CO|[:N/8yhm8B}C-:_B]N-|$~y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
